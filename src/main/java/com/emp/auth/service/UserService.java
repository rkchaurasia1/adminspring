package com.emp.auth.service;

import com.emp.auth.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
